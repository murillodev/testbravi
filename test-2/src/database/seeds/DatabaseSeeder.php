<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('people')->insert([
            'name' => 'Murillo Sampaio',
            'created_at' => new \DateTime(),
        ]);

        DB::table('contacts')->insert([
            'person_id' => 1,
            'contact' => 'phone',
            'value' => '(11) 95388-1601',
            'created_at' => new \DateTime(),
        ]);

        DB::table('contacts')->insert([
            'person_id' => 1,
            'contact' => 'email',
            'value' => 'murillosampaioleite@gmail.com',
            'created_at' => new \DateTime(),
        ]);

        DB::table('contacts')->insert([
            'person_id' => 1,
            'contact' => 'phone',
            'value' => '(11) 9999-9999',
            'created_at' => new \DateTime(),
        ]);

        DB::table('contacts')->insert([
            'person_id' => 1,
            'contact' => 'email',
            'value' => 'murillosampaioleite@gmail.com',
            'created_at' => new \DateTime(),
        ]);

        DB::table('contacts')->insert([
            'person_id' => 1,
            'contact' => 'whatsapp',
            'value' => '(11) 95388-1601',
            'created_at' => new \DateTime(),
        ]);
    }
}
