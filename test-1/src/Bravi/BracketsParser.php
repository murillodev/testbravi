<?php

namespace Bravi;

class BracketsParser
{
    public static function isBalanced($string)
    {
        $string = preg_replace("/([^\{\}\[\]\(\)])/", '', $string);
        $pair = ['{}', '[]', '()'];
        $length = strlen($string);

        $i = 0;
        $balanced = false;

        while ($i++ < $length) {
            $string = str_replace($pair, '', $string);
            if ($string == '') $balanced = true;
            if ($balanced) break;
        }

        return $balanced;
    }
}