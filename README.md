# Test 1

## Installing:

Clone this Repository.

===================================================================================

Go to test-1 folder: `cd test-1`

===================================================================================

Use docker (and docker-compose) to run it: `sudo docker-compose up -d`

===================================================================================

Install dependencies: `sudo docker exec -it test1_php composer install`

===================================================================================

Running tests: `sudo docker exec -it test1_php php vendor/bin/phpunit`

===================================================================================

# Test 2

## Installing:

Go to test-2 folder: `cd test-2`

===================================================================================

Use docker (and docker-compose) to run it: `sudo docker-compose up -d`

===================================================================================

Install dependencies: `sudo docker exec -it test2_web composer install`

===================================================================================

Run the migrate: `sudo docker exec -it test2_web php artisan migrate`

===================================================================================

Seed the database: `sudo docker exec -it test2_web php artisan db:seed`

===================================================================================

Check the environment `http://localhost:8888`

===================================================================================

##Endpoints
Listing all people: **GET**: `http://localhost:8888/api/people`

===================================================================================

Listing a single people: **GET**: `http://localhost:8888/api/people/1`

===================================================================================

Inserting a people:

**POST**: `http://localhost:8888/api/people`

**Body**:

`name `: Jose Ferreira

`contacts[contact][]` : phone

`contacts[value][]` : (11)99999-9999

`contacts[contact][]` : email

`contacts[value][]` : teste@teste.com

===================================================================================

Updating a people:

**PUT**: `http://localhost:8888/api/people/2`

**Body**:

`name`:Pessoa Teste 1 Editada

`contacts[id][]`:6

`contacts[contact][]`:phone

`contacts[value][]`:(11)99999-8888

`contacts[contact][]`:email

`contacts[value][]`:teste2@teste.com

===================================================================================

Deleting a People : **DELETE**: `http://localhost:8888/api/people/2`

===================================================================================

Deleting a Contact : **DELETE**: `http://localhost:8888/api/contacts/CONTACT_ID`

===================================================================================

# Test 3

## Installing:

Go to test-1 folder: `cd test-3`

===================================================================================

**Timber Plugin** was intalled via Admin Panel.

===================================================================================

The file `src/wp-content/themes/forecast/templates/page-home.twig` contains the HTML used for rendering the page itself, and

===================================================================================

the file `src/wp-content/themes/myforecast/assets/js/scripts.js` contains the Javascript used for retrieving the JSON from OpenWeatherMap.


